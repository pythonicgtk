import new

import pygtk, gtk

from pprint import pprint
from debug_print_statements import *


# These are functions that work on widgets in order to do 'magic'


def get_widget_by_name(container, widget_name):
    if 'get_children' not in dir(container):
        # no get_children() method => this is not a container
        raise TypeError

    containers = [container]

    while len(containers) != 0:
        container = containers.pop(0)
        for child in container.get_children():
            if child.get_name() == widget_name:
                return child
            if 'get_children' in dir(child):
                containers.extend(child.get_children())

    # haven't returned by now => not found
    return None

def set_properties(widget, properties_dict, ignore_errors=False):
    for property_name, property_value in properties_dict.iteritems():
        if ignore_errors:
            try:
                widget.set_property(property_name, property_value)
            except TypeError:
                pass
        else:
            widget.set_property(property_name, property_value)

class PythonicWidget():

    def __init__(self, *args, **kwargs):
        self.set_packing_vars(*args, **kwargs)
        self.attach_signal_handlers(**kwargs)
        self.set_name_param(**kwargs)

    def set_packing_vars(self, *args, **kwargs):
        if 'expand' not in kwargs and 'fill' not in kwargs:
            # if we're creating a widget that has no parent, then we should get
            # of this now or there'll be errors later
            return

        expand = kwargs.get("expand", True)
        fill = kwargs.get("fill", True)

        
        def set_packing(widget=None, old_parent=None):
            parent = self.get_parent()
            if parent is not None and 'query_child_packing' in dir(parent):
                old_values = list(parent.query_child_packing(self))
                # we just want to change the expand and fill
                old_values[0] = expand
                old_values[1] = fill
                parent.set_child_packing(self, expand, fill, old_values[2], old_values[3])

        set_packing()
        self.connect("parent-set", set_packing)

    def attach_signal_handlers(self, **cb_dict):
        events_to_signals = { 'on_click': 'clicked' }

        for event, func in cb_dict.items():
            if event in events_to_signals:
                self.connect(events_to_signals[event], func)

    def set_name_param(self, **kwargs):
        if 'name' in kwargs:
            self.set_name(kwargs['name'])
        
    @property
    def parent_window(self):
        if 'container' not in dir(self):
            return None

        parent = self.container
        while parent:
            if isinstance(parent, Window):
                return parent
            if 'container' not in dir(parent):
                return None
            parent = parent.container
        
        assert False, "Widget %r has no parent window" % self

    @property
    def child_widgets(self):
        if 'get_children' not in dir(self):
            yield None
            return

        widgets = [self]

        # When being called with self as a window, it is not returning widgets 

        while len(widgets) != 0:
            widget = widgets.pop(0)
            if not isinstance( widget, gtk.Container ):
                yield widget
                continue
            else:
                yield widget
                for child in widget.get_children():
                    yield child
                    if isinstance( child, gtk.Container ):
                        widgets.extend(child.get_children())


class Button(gtk.Button, PythonicWidget):
    def __init__(self, label=None, stock=None, *args, **kwargs):
        gtk.Button.__init__(self)
        PythonicWidget.__init__(self, *args, **kwargs)
        assert len(args) == 0
        if label == None:
            label = ""
        self.set_label(label)
        if stock != None:
            self.set_use_stock(True)
            self.set_label(stock)


class VBox(gtk.VBox, PythonicWidget):
    def __init__(self, *args, **kwargs):
        gtk.VBox.__init__(self)
        PythonicWidget.__init__(self, *args, **kwargs)
        for widget in args:
            widget.container = self
            self.pack_start(widget)

class HBox(gtk.HBox, PythonicWidget):
    def __init__(self, *args, **kwargs):
        gtk.HBox.__init__(self)
        PythonicWidget.__init__(self, *args, **kwargs)
        for widget in args:
            widget.container = self
            self.pack_start(widget)

class Label(gtk.Label, PythonicWidget):
    def __init__(self, text, *args, **kwargs):
        gtk.Label.__init__(self)
        PythonicWidget.__init__(self, *args, **kwargs)
        self.set_text(text)

class TextEntry(gtk.Entry, PythonicWidget):
    def __init__(self, *args, **kwargs):
        gtk.Entry.__init__(self)
        PythonicWidget.__init__(self, *args, **kwargs)

        if 'text' in kwargs:
            self.set_text(kwargs['text'])
    
    @property
    def value(self):
        return self.get_text()

    @property
    def text(self):
        return self.get_text()

class TextView(gtk.TextView, PythonicWidget):
    def __init__(self, *args, **kwargs):
        gtk.TextView.__init__(self)
        PythonicWidget.__init__(self, *args, **kwargs)
    
    @property
    def value(self):
        return self.get_text()

def Labeled( label, widget, **kwargs ):
    return HBox( Label(label), widget, **kwargs )

class ScrolledWindow(gtk.ScrolledWindow, PythonicWidget):
    def __init__(self, inside_widget, horizontal=None, vertical=None, **kwargs):
        gtk.ScrolledWindow.__init__(self)
        PythonicWidget.__init__(self, **kwargs)
        # FIXME should this be add_with_viewport? Or should it check
        # inside_widget to see if it should add_with_viewport
        if isinstance(inside_widget, gtk.Viewport):
            self.add(inside_widget)
        else:
            self.add_with_viewport(inside_widget)
        exiting_horizontal, existing_vertical = self.get_policy()
        if horizontal == 'always':
            self.set_policy(gtk.POLICY_ALWAYS, existing_vertical)
        elif horizontal == 'never':
            self.set_policy(gtk.POLICY_NEVER, existing_vertical)
        elif horizontal == 'auto' or horizontal == 'automatic':
            self.set_policy(gtk.POLICY_AUTOMATIC, existing_vertical)
        exiting_horizontal, existing_vertical = self.get_policy()
        if vertical == 'always':
            self.set_policy(exiting_horizontal, gtk.POLICY_ALWAYS)
        elif vertical == 'never':
            self.set_policy(exiting_horizontal, gtk.POLICY_NEVER)
        elif vertical == 'auto' or vertical == 'automatic':
            self.set_policy(exiting_horizontal, gtk.POLICY_AUTOMATIC)



class IconView(gtk.IconView, PythonicWidget):
    def __init__(self, elements, *args, **kwargs):
        gtk.IconView.__init__(self)
        PythonicWidget.__init__(self, *args, **kwargs)
        set_properties(self, kwargs, ignore_errors=True)
        # FIXME check the format of args
        self.__init_from_icon_set(elements)

    def __init_from_icon_set(self, elements):
        # elements should be a list. Each element should be the same.
        # Possible values:
        # string - each 'icon' is a text
        # (string, pixbuf) - standard icon and text
        list = None
        if type(elements[0]) == type(""):
            list = gtk.ListStore(str)
            for string in elements:
                list.append([string])
            self.set_model(list)
            self.set_text_column(0)
        elif type(elements[0]) == type(()):
            # FIXME finish this
            pass
        else:
            raise TypeError

class Toolbar(gtk.Toolbar, PythonicWidget):
    def __init__(self, *child_widgets, **kwargs):
        gtk.Toolbar.__init__(self)
        PythonicWidget.__init__(self, **kwargs)
        for child_widget in child_widgets:
            # insert it at the end
            #toolitem = gtk.ToolItem()
            #toolitem.pack_start(child_widget)
            self.insert( child_widget, -1 )

class NewButton(Button, PythonicWidget):
    def __init__(self, *args, **kwargs):
        Button.__init__(self, stock="gtk-new")
        PythonicWidget.__init__(self, *args, **kwargs)

class AddButton(Button, PythonicWidget):
    def __init__(self, *args, **kwargs):
        Button.__init__(self, stock="gtk-add")
        PythonicWidget.__init__(self, *args, **kwargs)

class RemoveButton(Button, PythonicWidget):
    def __init__(self, *args, **kwargs):
        Button.__init__(self, stock="gtk-remove")
        PythonicWidget.__init__(self, *args, **kwargs)

class SaveButton(Button, PythonicWidget):
    def __init__(self, *args, **kwargs):
        Button.__init__(self, stock="gtk-save")
        PythonicWidget.__init__(self, *args, **kwargs)

class NewToolbarButton(gtk.ToolButton, PythonicWidget):
    def __init__(self, *args, **kwargs):
        gtk.ToolButton.__init__(self, 'gtk-new')
        PythonicWidget.__init__(self, *args, **kwargs)

class PropertiesButton(Button, PythonicWidget):
    def __init__(self, *args, **kwargs):
        Button.__init__(self, stock="gtk-properties")
        PythonicWidget.__init__(self, *args, **kwargs)

class PropertiesToolbarButton(gtk.ToolButton, PythonicWidget):
    def __init__(self, *args, **kwargs):
        gtk.ToolButton.__init__(self, "gtk-properties")
        PythonicWidget.__init__(self, *args, **kwargs)

class ComboBox(gtk.ComboBox, PythonicWidget):
    def __init__(self, *rows, **kwargs):
        gtk.ComboBox.__init__(self)
        PythonicWidget.__init__(self, **kwargs)

        liststore = gtk.ListStore(str)
        self.set_model( liststore )

        cell = gtk.CellRendererText()
        self.pack_start( cell, True )
        self.add_attribute( cell, 'text', 0 )

        active = None

        for index, row in enumerate( rows ):
            if isinstance( row, basestring ):
                text = row
            else:
                text = row[0]
                options = row[1]
                if options['selected']:
                    active = index
            self.append_text( text )

        if active is not None:
            self.set_active( active )


class Notebook(gtk.Notebook, PythonicWidget):
    def __init__(self, *pages, **kwargs):
        gtk.Notebook.__init__(self)
        PythonicWidget.__init__(self, **kwargs)

        for page_name, page_contents in pages:
            if isinstance(page_name, basestring):
                page_name = Label( page_name )
            self.append_page( child=page_contents, tab_label=page_name )

class RadioButton(gtk.RadioButton, PythonicWidget):
    def __init__(self, group=None, label=None, *args, **kwargs):
        gtk.RadioButton.__init__(self, group=None, label=label)
        PythonicWidget.__init__(self, *args, **kwargs)
        self.group = group
        self.label = label
        if 'value' in kwargs:
            self.value = kwargs['value']

        
        def update_all_radiobutton_groups(widget, new_child):
            if 'get_children' not in dir( widget ):
                return

            child_radiobuttons = [child for child in widget.child_widgets if isinstance( child, RadioButton ) ]

            groups = {}
            for radiobutton in child_radiobuttons:
                group_name = radiobutton.group
                if group_name not in groups:
                    groups[group_name] = []
                groups[group_name].append(radiobutton)

            for group_name in groups:
                inital_group = groups[group_name][0]
                if len(groups[group_name]) <= 1:
                    continue
                for radiobutton in groups[group_name][1:]:
                    radiobutton.set_group(None)
                    radiobutton.set_group(inital_group)


        def add_updater_callback(widget, old_parent):
            #print "Adding the add_updater_callback to "+repr(widget)
            # when the parent is set we need to tell the parent to update_all_radiobutton_groups
            
            if "get_children" in dir( widget ):
                #print repr(widget)+" is a container"
                update_all_radiobutton_groups(widget, None)
                widget.connect( "add", update_all_radiobutton_groups )

            if 'container' in dir( widget ) and widget.container:
                widget.container.connect( "parent-set", add_updater_callback )

        self.connect("parent-set", add_updater_callback)
        update_all_radiobutton_groups(self, None)



class ListBox(gtk.TreeView, PythonicWidget):
    def __init__(self, *rows, **kwargs):
        try:
            columns = kwargs['columns']
        except KeyError:
            # Turn our key error into a type error
            raise TypeError, "ListBox constructor requires the 'columns' argument"
        gtk.TreeView.__init__(self)
        PythonicWidget.__init__(self, **kwargs)

        # We assume they are all text columns for now
        # TODO allow the caller to change this.
        for index, col in enumerate(columns):
            if isinstance( col, basestring ):
                type = str
            else:
                pass
            column = gtk.TreeViewColumn(col, gtk.CellRendererText(), text=index)
            column.set_resizable(True)
            column.set_sort_column_id(index)
            self.append_column(column)

        #view_entries_list.connect("row-activated", show_single_entry)
        
        #column_types = []
        #column_types.extend([x['type'] for x in view_def['columns']])
        #column_types.extend([gobject.TYPE_PYOBJECT])
        # TODO more types
        column_types = [str] * len(columns)
        self.rows = ListStore(column_types=column_types, rows=rows)

        self.set_model(self.rows)

class ListStore(gtk.ListStore, PythonicWidget):
    def __init__(self, column_types, rows=None, *args, **kwargs):
        gtk.ListStore.__init__(self, *column_types)
        PythonicWidget.__init__(self, *args, **kwargs)
        self.column_types = column_types
        for row in rows:
            if len(row) != len(column_types):
                raise TypeError, "Row %r has %d entries but there are %d columns definied (%r)" % ( row, len(row), len(columns), columns)
            gtk.ListStore.append(self, row)


class Window(gtk.Window, PythonicWidget):
    def __init__(self, child_widget, *args, **kwargs):
        gtk.Window.__init__(self)
        PythonicWidget.__init__(self, **kwargs)

        self.add(child_widget)
        child_widget.container = self

        if 'title' in kwargs:
            self.set_title(kwargs['title'])

        if 'quit_on_close' in kwargs:
            if kwargs['quit_on_close'] == True:
                self.connect("destroy", gtk.main_quit)

    def get_value(self, value_name):
        # Either the widget is a text box (eg) that we can read the value off,
        # or it's some kind of multi-selection thing, like a set of radio
        # buttons
        widget_by_name = get_widget_by_name(self, value_name)
        if widget_by_name:
            return widget_by_name.value
        else:
            widget_by_group = [widget for widget in self.child_widgets if isinstance( widget, RadioButton ) and widget.group == value_name]
            if len(widget_by_group) == 0:
                # no radio buttons have value_name as a group name
                return
            for widget in widget_by_group:
                if widget.get_active():
                    if 'value' in dir(widget):
                        return widget.value
                    else:
                        return widget.label
            
            return None

class Dialog(gtk.Dialog, Window, PythonicWidget):
    def __init__(self, central_area, button_area, *args, **kwargs):
        title = None
        if 'title' in kwargs:
            title = kwargs['title']
        
        gtk.Dialog.__init__(self, title=title, buttons=button_area)
        PythonicWidget.__init__(self)

        self.vbox.pack_start(central_area)
        self.central_area = central_area

    def wait_for_response(self):
        self.show_all()
        response = self.run()
        self.destroy()
        return response

    def get_children(self):
        # if we don't have this get_widget_by_name won't work. We'll just skip
        # the vbox in the middle. We (probably) don't care about the buttons at
        # the bottom
        return [self.central_area]

def FileChooser(filetypes=None):
    chooser = gtk.FileChooserDialog(buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
    if filetypes is not None:
        for filetype in filetypes:
            # TODO allow the user to change the name of the filter
            filter = gtk.FileFilter()
            filter.add_pattern(filetype)
            chooser.add_filter(filter)
    chooser.show_all()
    chooser.run()
    response = chooser.run()
    if response == gtk.RESPONSE_OK:
        result = chooser.get_filename()
    elif response == gtk.RESPONSE_CANCEL:
        result =  None
    chooser.destroy()
    return result
    

def main():
    gtk.main()
